import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 初始化elementUi
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)



// 引入jquery
import $ from 'jquery'

Vue.prototype.$ = $;   // 当然还有这句话 给vue原型上添加 $

import parser from '@/api/js/parser.js';
Vue.prototype.parser = parser;
import mselector from '@/api/js/mselector.js';
Vue.prototype.mselector = mselector; 

import mtags from '@/api/js/mtags.js';
Vue.prototype.mtags = mtags; 

//HttpHandler 暴露在全局
import { HttpHandler } from "@/api/Gener.js";
Vue.prototype.HttpHandler = HttpHandler;

import { DBAccess } from "@/api/Gener.js";
Vue.prototype.DBAccess = DBAccess;

import { Entities } from "@/api/Gener.js";
Vue.prototype.Entities = Entities;

import { Entity } from "@/api/Gener.js";
Vue.prototype.Entity = Entity;

import { GetPara } from "@/api/Gener.js";
Vue.prototype.GetPara = GetPara;

Vue.config.productionTip = false

//设置全局广播事件
Vue.prototype.$Bus= Vue.prototype.$Bus || new Vue();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')



