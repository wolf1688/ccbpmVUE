﻿import $ from 'jquery'
// UI风格配置. UIPlant, 为了适应不同风格的版本需要. 我们增加这个配置, UIPlant=BS,Ele.
var uiPlant = 'BS'; //风格文件.

//  For .net 后台的调用的url ,  java的与.net的不同.
var plant = 'CCFlow'; //运行平台.
var Handler = "Handler.ashx"; //处理器,一般来说，都放在与当前处理程序的相同的目录下。
var MyFlow = "MyFlow.ashx"; //工作处理器.
//
//var plant = 'JFlow'; //运行平台.
//var Handler = "ProcessRequest.do"; //处理器,一般来说，都放在与当前处理程序的相同的目录下。
//var MyFlow = "MyFlow/ProcessRequest.do"; //工作处理器.

var webUser = null;//定义通用变量用户信息
var dynamicHandler = '';

var basePath='';
var webPath="http://localhost:8080/jflow-web";


/**
 * 获取项目路径
 * @returns
 */
function getContextPath() {
    return basePath.substring(basePath.lastIndexOf("/"));
}

//公共方法
function Handler_AjaxQueryData(param, callback, scope, method) {
    if (!method) method = 'GET';
    $.ajax({
        type: method, //使用GET或POST方法访问后台
        dataType: "text", //返回json格式的数据
        contentType: "text/plain; charset=utf-8",
        url: Handler, //要访问的后台地址.
        data: param, //要发送的数据.
        async: true,
        cache: false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        complete: function(){ }, //AJAX请求完成时隐藏loading提示
        error: function (XMLHttpRequest) {
            callback(XMLHttpRequest);
        },
        success: function (msg) {//msg为返回的数据，在这里做数据绑定
            var data = msg;
            callback(data, scope);
        }
    });
}
export {
    uiPlant,
    plant,
    dynamicHandler,
    basePath,
    webPath,
    Handler,
    MyFlow,
    webUser,
    getContextPath,
    Handler_AjaxQueryData
};

// for Java.
// var controllerURLConfig = '/WF/Admin/CCFormDesigner/CCFromHandler';
// var plant = 'JFlow'; //运行平台.
// var Handler = "Handler";




