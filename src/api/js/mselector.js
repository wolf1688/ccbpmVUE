﻿import $ from 'jquery';
import {DBAccess} from '../Gener.js';

(function () {

    function onSelect(target, record) {
        var opts = getOptions(target);
        opts.onSelect.call("", record);
    }

    function onUnselect(target, record) {
        var opts = getOptions(target);
        opts.onUnselect.call("", record);
    }

    function loadData(target, datas) {
        var opts = getOptions(target);
        var search = $(target).find(".ccflow-search");
        var valueField = opts.valueField;
        var textField = opts.textField;
        for (var i = 0; i < datas.length; i++) {
            var data = datas[i];
            if (!contains(target, data, valueField) && data[valueField] != empty && typeof data[valueField] !== "undefined") {
                var tag = $('<span class="el-tag el-tag--info el-tag--small el-tag--light" style="margin-left:3px"></span>');
                tag.data(data);
                tag.html( data[textField] + '<i class="el-tag__close el-icon-close" data-role="remove"></i>');
                search.before(tag);
                onSelect(target, data);
                tag.delegate("i", "click", function () {
                    var record = $(this).parent().data();
                    $(this).parent().remove();
                    opts.onUnselect.call("", record);
                });
            }
        }
        var container = $(target).find(".ccflow-input-span-container");
     
        var elInput =$(target).find(".el-input__inner");
        elInput.css({"height":container.height()})
    }

    function clear(target) {
        $(target).find(".ccflow-input-span-container span").remove();
    }

    function setValues(target, values) {
        var opts = getOptions(target);
        var valueField = opts.valueField;
        var textField = opts.textField;
        var datas = [];
        if ($.isArray(opts.data) && opts.data.length > 0) {
            datas = opts.data;
        } else if ($.trim(opts.sql) != "") {
            datas = executeSql(opts.sql, opts.valueField, opts.textField, values);
        } else if ($.trim(opts.url) != "") {
            datas = requestUrl(opts.url, valueField, textField);
        }
        loadData(target, datas);
    }

    function getText(target) {
        var opts = getOptions(target);
        var textField = opts.textField;
        var text = [];
        $(target).find(".ccflow-input-span-container span").each(function () {
            text.push($(this).data()[textField]);
        });
        return text.join(",");
    }

    function getValue(target) {
        var opts = getOptions(target);
        var valueField = opts.valueField;
        var text = [];
        $(target).find(".ccflow-input-span-container span").each(function () {
            text.push($(this).data()[valueField]);
        });
        return text.join(",");
    }

    function getOptions(target) {
        return $.data(target, "mselector").options;
    }

    var empty = "__empty__";

    function requestUrl(url, valueField, textField) {
        var datas = [];
        if (url && $.trim(url) != "") {
            $.ajax({
                type: 'post',
                async: false,
                url: url + "&t=" + new Date().getTime(),
                dataType: 'html',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                success: function (data) {
                    if (data.indexOf("err@") != -1) {
                        alert(data);
                        return;
                    }
                    var ja = JSON.parse(data);
                    if ($.isArray(ja)) {
                        $.each(ja, function (i, o) {
                            var option = {};
                            option[valueField] = o[valueField];
                            option[textField] = o[textField];
                            datas.push(option);
                        });
                    }
                },
                error: function (XMLHttpRequest) {
                    alert("系统发生异常, status: " + XMLHttpRequest.status + " readyState: " + XMLHttpRequest.readyState);
                }
            });
        }
        return datas;
    }

    function executeSql(sql, valueField, textField, key) {
        var datas = [];
        if (sql && $.trim(key) != "") {
            key = key.replace("'", "");
            var _sql = sql.replace(/@Key/g, key).replace(/~/g, "'");
            var dt = DBAccess.RunSQLReturnTable(_sql);
            if ($.isArray(dt)) {
                $.each(dt, function (i, o) {
                    var option = {};
                    option[valueField] = o[valueField];
                    option[textField] = o[textField];
                    datas.push(option);
                });
            }
        }
        return datas;
    }

    function contains(target, data, valueField) {
        var flag = false;
        $(target).find(".ccflow-input-span-container span").each(function () {
            if (data[valueField] == $(this).data()[valueField]) {
                flag = true;
                return;
            }
        });
        return flag;
    }

    function create(target) {
        var opts = getOptions(target);
        var html = "";
        html += '<div class="el-select main-container">';
        html += '<div class="el-select__tags ccflow-input-span-container" style="margin-top:5px">';
        html += '<input type="text" class="ccflow-search el-select__input" >';
        html += '</div>';
        html += '<div class="el-input el-input--suffix">';
        html += '<input tabindex="-1" type="text" readonly="readonly" autocomplete="off" placeholder="" class="el-input__inner" style="height: 40px;">';
        html += '</div>';
        html += '</div>';
        
  
        html += '<div class="ccflow-block el-scrollbar el-select-dropdown" style="display:none">';
        html += '<div class="el-select-dropdown__wrap el-scrollbar__wrap" style="margin-bottom: -21px; margin-right: -21px;">';
        html += '<ul class="ccflow-ul el-scrollbar__view el-select-dropdown__list">';
        html += '</ul>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
  
        $(target).html(html);

        var valueField = opts.valueField;
        var textField = opts.textField;
        var emptyOption = {};
        emptyOption[valueField] = empty;
        emptyOption[textField] = "无匹配项";

        var search = $(target).find(".ccflow-search");
        var container = $(target).find(".ccflow-input-span-container");
        var block = $(target).find(".ccflow-block");
        var ul = $(target).find(".ccflow-ul");
        var elInput =$(target).find(".el-input__inner");
       

        search.focus(function () {
            animteDown();
        });

        ul.blur(function () {
            setTimeout(function () {
                animateUp(container, block);
            }, 200);
        });

        function addDictionary(datas, callback) {
            for (var i = 0; i < datas.length; i++) {
                var data = datas[i];
                var li = $("<li class='el-select-dropdown__item'></li>");
                li.text(data[textField]);
                li.data(datas[i]);
				li.attr("tabindex", i);
                ul.append(li);
            }
            callback(data, valueField);
        }

        function updateDictionary(datas, callback) {
            ul.empty();
            addDictionary(datas, callback);
        }

       function animteDown() {
            block.slideDown("fast");
            
        }

        search.keyup(function () {
            var text = search.val();
            var datas = [];
            var src = opts.dbSrc;
            text = text.replace("'", "");
            //增加数据源的访问.
            src = src.replace(/@Key/g, text).replace(/~/g, "'");
            var dt = DBAccess.RunDBSrc(src);

            if ($.isArray(dt)) {
                $.each(dt, function (i, o) {
                    var option = {};
                    option[valueField] = o[valueField];
                    option[textField] = o[textField];
                    datas.push(option);
                });
            }

            if (opts.filter) {
                datas = $.grep(datas, function (o) {
                    return o[textField].indexOf(text) != -1;
                });
                if (datas.length === 0) {
                    datas.push(emptyOption);
                }
            }
            updateDictionary(datas, addListener);
			
			
        });

        function addListener() {
            ul.delegate("li", "click", function () {
                var data = $(this).data();
                if (!contains(target, data, valueField) && data[valueField] != empty) {
                    var tag = $('<span class="el-tag el-tag--info el-tag--small el-tag--light"></span>');
                    tag.data(data);
                    tag.html($(this).text() + '<i class="el-tag__close el-icon-close" data-role="remove"></i>');
                    search.before(tag);
                    onSelect(target, data);
                    tag.delegate("i", "click", function () {
                        var record = $(this).parent().data();
                        $(this).parent().remove();
                        opts.onUnselect.call("", record);
                    });
                }
                search.val("");
                animateUp();
            });

			ul.children("li").on("mouseover", function () {
				ul.children("li").removeClass("hover");
				$(this).addClass("hover");
			});


        }

        function animateUp() {
            block.slideUp("fast", function () {
                elInput.css({"height":container.height()});
                
            });
        }
    }

    
    /*function setSize(target) {
        var opts = getOptions(target);
        var t = $(target);
        if (opts.fit == true) {
            var p = t.parent();
            opts.width = p.width();
        }
        
        t._outerWidth(opts.width);
        resize(target);
        $(window).bind("resize", function () {
            resize(target);
        });
    }*/

    $.fn.mselector = function (options, params) {
        if (typeof options == 'string') {
            return $.fn.mselector.methods[options](this, params);
        }
        options = options || {};
        return this.each(function () {
            var state = $.data(this, "mselector");
            if (state) {
                $.extend(state.options, options);
            } else {
                state = $.data(this, 'mselector', {
                    options: $.extend({}, $.fn.mselector.defaults, $.fn.mselector.parseOptions(this), options)
                });
                create(this);
            }
            //setSize(this);
        });
    };

    $.fn.mselector.methods = {
        setValues: function (jq, values) {
            return jq.each(function () {
                setValues(this, values);
            });
        },
        getValue: function (jq) {
            return getValue(jq[0]);
        },
        getText: function (jq) {
            return getText(jq[0]);
        },
        clear: function (jq) {
            return jq.each(function () {
                clear(this);
            });
        },
        loadData: function (jq, values) {
            return jq.each(function () {
                loadData(this, values);
            });
        }
    };

    $.fn.mselector.parseOptions = function (target) {
        return $.extend({}, $.parser.parseOptions(target, ["width", "data", {
            "fit": "boolean",
            "valueField": "string",
            "textField": "string",
            "label": "string",
            "filter": "boolean",
            "dbSrc": "string"
        }]));
    };

    $.fn.mselector.defaults = {
        "width": "100%",
        "fit": true,
        "valueField": "No",
        "textField": "Name",
        "label": "",
        "filter": true,
        "tip": "请输入关键字",
        "dbSrc": "",
        "onSelect": function () {
        },
        "onUnselect": function () {
            onUnselect();
        }
    };

})();



