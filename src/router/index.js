import Vue from 'vue'
import VueRouter from 'vue-router'
import home from '../wf/appclassic/home.vue'
import login from '../wf/appclassic/login.vue'
import myflow from '../wf/myFlow.vue'
import MyFlowGener from '../wf/myFlowGener.vue'
import MyFlowSelfForm from '../wf/myFlowSelfForm.vue'
import MyViewSelfForm from '../wf/myViewSelfForm.vue'
import MyView from '../wf/myView.vue'
import MyViewGener from '../wf/myViewGener.vue'
import start from '../wf/start.vue' //发起
import todolist from '../wf/todolist.vue' //待办
import runing from '../wf/runing.vue' // 在途
import complete from '../wf/complete.vue' //已完成
import demo from '../demo/demo.vue' //已完成
import Search from '../wf/comm/search.vue'
import EnOnly from '../wf/comm/enOnly.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: home,
    children: [
      {
        path: '/start',
        name: 'start',
        component: start,
      },
      {
        path: '/todolist',
        name: 'todolist',
        component: todolist,
      },
      {
        path: '/runing',
        name: 'runing',
        component: runing,
      },
      {
        path: '/complete',
        name: 'complete',
        component: complete,
      },
      {
        path:'/myflow',
        name: 'myflow',
        component: myflow
      },
      {
        path:'/MyFlowGener',
        name: 'MyFlowGener',
        component: MyFlowGener
      },
      {
        path:'/MyFlowSelfForm',
        name: 'MyFlowSelfForm',
        component: MyFlowSelfForm
      },
      {
        path:'/MyViewSelfForm',
        name: 'MyViewSelfForm',
        component: MyViewSelfForm
      },
      {
        path:'/MyView',
        name: 'MyView',
        component: MyView
      },
      {
        path:'/MyViewGener',
        name: 'MyViewGener',
        component: MyViewGener
      },
      {
        path:'/search',
        name:'search',
        component:Search
      },
      {
        path:'/enOnly',
        name:'enOnly',
        component:EnOnly
      }
    ]
  },
  {
    path: '/',
    name: 'login',
    component: login
  },
  
  {
    path:'/demo',
    name: 'demo',
    component: demo
  }
]

const router = new VueRouter({
  mode: 'history', //项目发布的时候需要设置成hash,否则发布项目运行会出现空白
  routes
})

export default router
